<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;

use App\Models\Invitation;
use App\Jobs\SendInvitationFeedback;

class InvitationFeedbackMinutesUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invitation-feedback-minutes:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending feedback to all users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $invitations = Invitation::where('feedback_sended', 0)
            ->where('registered_at', '!=', null)
            ->get();

        foreach ($invitations as $invitation) {
            if (($invitation->registered_at && !$invitation->feedback_sended) && Carbon::now()->diffInHours($invitation->registered_at) == 1) {
                $invitation->update(['feedback_sended' => 1]);

                SendInvitationFeedback::dispatch($invitation);
            }

        }
    }
}
