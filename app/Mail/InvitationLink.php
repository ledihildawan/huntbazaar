<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvitationLink extends Mailable
{
    use Queueable, SerializesModels;

    public $invitation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invitation)
    {
        $this->invitation = $invitation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = route('invitation.link', $this->invitation);
        $hour = date('H');
        $timeOfDay = ($hour > 17) ? 'evening' : (($hour > 12) ? 'afternoon' : 'morning');

        return $this->from(env('MAIL_USERNAME'), env('APP_NAME'))
            ->markdown('emails.invitation.link', [
                'invitation' => $this->invitation,
                'timeOfDay' => $timeOfDay,
                'url' => $url,
            ]);
    }
}
