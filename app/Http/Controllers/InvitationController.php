<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Models\Designer;
use App\Models\Invitation;
use App\Jobs\SendInvitationLink;
use App\Http\Requests\StoreInvitationRequest;
use App\Http\Requests\UpdateInvitationRequest;
use App\Http\Requests\UpdateInvitationWebRequest;

use Illuminate\Support\Facades\Validator;

class InvitationController extends Controller
{
    public function index(Request $request)
    {
        if ($request['date'] && $request['type'] === 'total-sent-today' && $request['order-by'] === 'status') {
            $invitations = Invitation::join('statuses', 'invitations.status_id', '=', 'statuses.id')
                ->select('invitations.*')
                ->whereDate('invitations.created_at', Carbon::today())
                ->where('invitations.email', 'LIKE', "%{$request->q}%")
                ->where('invitations.created_at', '!=', null)
                ->where('invitations.status_id', 1)
                ->orderBy('statuses.name', $request->order ?? 'asc')
                ->paginate(10);
        } else if ($request['date'] && $request['type'] === 'total-registered-today' && $request['order-by'] === 'status') {
            $invitations = Invitation::join('statuses', 'invitations.status_id', '=', 'statuses.id')
                ->select('invitations.*')
                ->whereDate('invitations.registered_at', Carbon::today())
                ->where('invitations.email', 'LIKE', "%{$request->q}%")
                ->where('invitations.registered_at', '!=', null)
                ->where('invitations.status_id', 2)
                ->orderBy('statuses.name', $request->order ?? 'asc')
                ->paginate(10);
        } else if ($request['type'] === 'total-registered' && $request['order-by'] === 'status') {
            $invitations = Invitation::join('statuses', 'invitations.status_id', '=', 'statuses.id')
                ->select('invitations.*')
                ->where('invitations.email', 'LIKE', "%{$request->q}%")
                ->where('invitations.registered_at', '!=', null)
                ->where('invitations.status_id', 2)
                ->orderBy('statuses.name', $request->order ?? 'asc')
                ->paginate(10);
        } else if ($request['type'] === 'total-sent' && $request['order-by'] === 'status') {
            $invitations = Invitation::join('statuses', 'invitations.status_id', '=', 'statuses.id')
                ->select('invitations.*')
                ->where('invitations.email', 'LIKE', "%{$request->q}%")
                ->where('invitations.created_at', '!=', null)
                ->where('invitations.status_id', 1)
                ->orderBy('statuses.name', $request->order ?? 'asc')
                ->paginate(10);
        } else if ($request['date'] && $request['type'] === 'total-registered-today') {
            $invitations = Invitation::whereDate('registered_at', Carbon::today())
                ->where('name', 'LIKE', "%{$request->q}%")
                ->where('registered_at', '!=', null)
                ->where('status_id', 2)
                ->orderBy($request['order-by'] ?? 'registered_at', $request->order ?? 'asc')
                ->paginate(10);
        } else if ($request['date'] && $request['type'] === 'total-sent-today') {
            $invitations = Invitation::whereDate('created_at', Carbon::today())
                ->where('name', 'LIKE', "%{$request->q}%")
                ->where('created_at', '!=', null)
                ->where('status_id', 1)
                ->orderBy($request['order-by'] ?? 'registered_at', $request->order ?? 'asc')
                ->paginate(10);
        } else if ($request['type'] === 'total-registered') {
            $invitations = Invitation::where($request->column ?? 'email', 'LIKE', "%{$request->q}%")
                ->where('registered_at', '!=', null)
                ->where('status_id', 2)
                ->orderBy($request['order-by'] ?? 'registered_at', $request->order ?? 'asc')
                ->paginate(10);
        } else if ($request['type'] === 'total-sent') {
            $invitations = Invitation::where($request->column ?? 'email', 'LIKE', "%{$request->q}%")
                ->where('created_at', '!=', null)
                ->where('status_id', 1)
                ->orderBy($request['order-by'] ?? 'registered_at', $request->order ?? 'asc')
                ->paginate(10);
        } else if ($request['order-by'] === 'status') {
            $invitations = Invitation::join('statuses', 'invitations.status_id', '=', 'statuses.id')
                ->select('invitations.*')
                ->orderBy('statuses.name', $request->order ?? 'asc')
                ->paginate(10);
        } else if ($request['q']) {
            $invitations = Invitation::where($request->column, 'LIKE', "%{$request->q}%")
                ->orderBy($request['order-by'] ?? 'registered_at', $request->order ?? 'asc')
                ->paginate(10);
        } else {
            $invitations = Invitation::latest()->paginate(10);
        }

        return view('app.invitation.index', compact('invitations'));
    }

    public function create()
    {
        return view('app.invitation.create');
    }

    public function store(StoreInvitationRequest $request)
    {
        $invitation = Invitation::create(array_merge($request->all(), [
            'link' => Str::random(64),
            'status_id' => 1,
        ]));

        SendInvitationLink::dispatch($request->email, $invitation);

        return redirect()->route('app.invitation.index');
    }

    public function show(Invitation $invitation)
    {
        return view('app.invitation.show', compact('invitation'));
    }

    public function edit(Invitation $invitation)
    {
        return view('app.invitation.edit', compact('invitation'));
    }

    public function update(UpdateInvitationRequest $request, Invitation $invitation)
    {
        $invitation->update(array_merge($request->all(), [
            'link' => Str::random(64),
            'status_id' => 1,
            'name' => null,
            'birth_date' => null,
            'gender' => null,
            'designers_favorites' => null,
            'registration_code' => null,
            'registered_at' => null,
            'designers_favorites' => null,
            'feedback_sended' => 0,
        ]));

        SendInvitationLink::dispatch($request->email, $invitation);

        return redirect()->route('app.invitation.index');
    }

    public function destroy(Invitation $invitation)
    {
        $invitation->delete();

        return redirect()->route('app.invitation.index');
    }

    public function invitation(Invitation $invitation)
    {
        $invitation = Invitation::where('link', $invitation->link)->first();
        $designers = Designer::all();

        $now = Carbon::now();
        $start = Carbon::create($invitation->created_at);
        $end = Carbon::create($invitation->expiration_date);
        $isRegistrationStillOpen = $now->between($start, $end);

        if (!$invitation) {
            abort(404);
        }

        return view('invitation', compact('invitation', 'designers', 'isRegistrationStillOpen'));
    }

    public function invitationUpdate(UpdateInvitationWebRequest $request, Invitation $invitation)
    {
        dd($request->email);
        $invitation->update(array_merge($request->all(), [
            'status_id' => 2,
            'registration_code' => Str::random(8),
            'registered_at' => now(),
            'designers_favorites' => implode(', ', $request->designers_favorites),
        ]));

        return redirect()->route('invitation.link', $invitation);
    }
}
