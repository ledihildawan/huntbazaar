<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Models\Invitation;

class DashboardController extends Controller
{
    public function index()
    {
        $totalInvitationSent = Invitation::where('status_id', 1)->count();
        $totalInvitationSentToday = Invitation::where('status_id', 1)
            ->whereDate('created_at', Carbon::today())
            ->count();
        $totalInvitationRegistered = Invitation::where('status_id', 2)->count();
        $totalInvitationRegisteredToday = Invitation::where('status_id', 2)
            ->whereDate('registered_at', Carbon::today())
            ->count();

        return view('app.index', compact(
            'totalInvitationSent',
            'totalInvitationSentToday',
            'totalInvitationRegistered',
            'totalInvitationRegisteredToday',
        ));
    }
}
