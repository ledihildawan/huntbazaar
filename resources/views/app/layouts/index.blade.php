<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />

  <title>@yield('title')</title>

  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700&display=swap" rel="stylesheet" />

  <style>
    * {
      box-sizing: border-box;
    }

    body {
      margin: 0;
      background: #F3F4F6;
      font-family: 'Nunito', sans-serif;
    }

    table {
      width: 100%;
      border-collapse: collapse;
      border-radius: .25rem;
      overflow: hidden;
      white-space: nowrap;
    }

    table td,
    table th {
      padding: .5rem;
    }

    table tr:hover {
      background-color: #f3f4f6;
    }

    table th {
      padding-top: .625rem;
      padding-bottom: .625rem;
      text-align: left;
      background-color: #4A4E69;
      color: white;
    }

    table th > div {
      display: flex;
      align-items: center;
    }

    table th a {
      color: #fff;
      text-decoration: none;
    }

    table th svg {
      height: .75rem;
      margin-left: .5rem;
    }

    .container {
      max-width: 1200px;
      margin-left: auto;
      margin-right: auto;
      padding: 0 1rem;
    }

    .countdown {
      margin-bottom: 2rem;
      min-height: 24px;
    }

    .header {
      background: #fff;
      padding: 1rem 0;
    }

    .header__container {
      display: flex;
      align-items: center;
      justify-content: space-between
    }

    .logo__text {
      font-weight: 700;
      letter-spacing: 1px;
      text-decoration: none;
      color: inherit;
    }

    .nav__list {
      margin: 0;
      padding: 0;
      display: flex;
      list-style-type: none;
    }

    .nav__list-item:not(:last-child) {
      margin-right: 1rem;
    }

    .nav__link {
      text-decoration: none;
      color: inherit;
      font-weight: 700;
    }

    .nav__link:hover {
      text-decoration: underline;
    }

    .logout button {
      appearance: none;
      background: none;
      border: 0;
      font-family: inherit;
      font-size: 1rem;
      font-weight: 700;
      cursor: pointer;
    }

    .logout button:hover {
      text-decoration: underline;
    }

    .main {
      padding-top: 3rem;
      padding-bottom: 3rem;
    }

    .card__header .container {
      display: flex;
      flex-direction: column-reverse;
      gap: 1rem;
    }

    .card__control {
      background: #fff;
      padding: .5rem 1rem;
      border-radius: .25rem;
      display: flex;
      flex-direction: column;
      gap: .5rem;
    }

    .card__title {
      margin: 0;
    }

    .card__link {
      background: #22223B;
      color: #fff;
      text-decoration: none;
      padding: .625rem 1rem;
      border-radius: .25rem;
      text-align: center;
    }

    .card__content {
      margin-top: 2rem;
      background-color: #fff;
      padding: 1rem;
      border-radius: .25rem;
    }

    .card__details p:first-child {
      margin: 0 0 .25rem;
    }

    .card__details p:last-child {
      margin: 0 0 .5rem;
      font-weight: 700;
    }

    .form-group {
      margin-bottom: 1rem;
    }

    .form-group label {
      display: block;
    }

    .form-control {
      display: block;
      width: 100%;
      font-size: 1rem;
      font-family: inherit;
      border: 2px solid #f3f4f6;
      background-color: #f3f4f6;
      padding: .5rem;
      border-radius: .25rem;
      color: inherit;
    }

    .form-control.error {
      border-color: #FF312E;
    }

    .form-control.inline {
      width: initial;
      display: inline-block;
    }

    .form-control:focus {
      outline: 0;
    }

    .btn {
      appearance: none;
      background: #22223B;
      border: 0;
      font-family: inherit;
      font-size: 1rem;
      font-weight: 400;
      cursor: pointer;
      color: #fff;
      padding: .625rem 1rem;
      border-radius: .25rem;
      width: 100%;
    }

    .dashboard__container {
      display: flex;
      flex-direction: column;
      gap: 1rem;
    }

    .dashboard__card {
      background: #fff;
      padding: 1rem;
      border-radius: .25rem;
    }

    .dashboard__link {
      text-decoration: none;
      color: inherit;
    }

    .dashboard__card h2 {
      margin: 0;
      font-size: 1rem;
    }

    .dashboard__card p {
      margin: .5rem 0 0;
      font-size: 2rem;
      font-weight: 700;
    }

    .actions {
      display: flex;
      gap: 1rem;
    }

    .actions svg {
      stroke: #000;
      height: 1rem;
      width: 1rem;
    }

    .action-delete {
      appearance: none;
      background: none;
      border: 0;
      font-family: inherit;
      font-size: 1rem;
      cursor: pointer;
      padding: 0;
      margin: 0;
    }

    .no-data {
      padding: 1rem;
      text-align: center;
    }

    .text-error {
      color: #FF312E;
    }

    .invitation-status {
      padding: .15rem .5rem;
      border-radius: .25rem;
      color: #fff;
      font-weight: 500;
    }

    .invitation-status.sent {
      background-color: #A4BEF3;
      color: #000;
    }

    .invitation-status.registered {
      background-color: #06D6A0;
      color: #fff;
    }

    @media (min-width: 992px) {
      html {
        font-size: 112.5%;
      }

      .card__header .container {
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
      }

      .card__control {
        flex-direction: row;
      }

      .dashboard__container {
        flex-direction: row;
      }

      .btn {
        width: initial;
      }
    }
  </style>

  @stack('styles')

</head>
<body>
  @include('app.components.header')

  <main class="main">
    @yield('content')
  </main>

  @stack('scripts')

</body>
</html>