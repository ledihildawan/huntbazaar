@extends('app.layouts.index')

@section('title', 'Dashboard | ' . env('APP_NAME'))

@section('content')
  <div class="dashboard">
    <div class="container dashboard__container">
      <a
      class="dashboard__link"
        href="{{ route('app.invitation.index') . '?type=total-registered-today&order-by=registered_at&order=desc&date=' . date('Y-m-d') }}"
        >
        <div class="dashboard__card">
          <h2>The Total Number of Invitees Who Have Registered Today</h2>
          <p>{{ $totalInvitationRegisteredToday }}</p>
        </div>
      </a>
      <a
      class="dashboard__link"
        href="{{ route('app.invitation.index') . '?type=total-sent-today&order-by=registered_at&order=desc&date=' . date('Y-m-d') }}"
        >
        <div class="dashboard__card">
          <h2>Total Number of Invitations Sent Today</h2>
          <p>{{ $totalInvitationSentToday }}</p>
        </div>
      </a>
      <a
      class="dashboard__link"
        href="{{ route('app.invitation.index') . '?type=total-registered&order-by=registered_at&order=desc' }}"
        >
        <div class="dashboard__card">
          <h2>Total Number of Invitations Who Have Registered</h2>
          <p>{{ $totalInvitationRegistered }}</p>
        </div>
      </a>
      <a
      class="dashboard__link"
        href="{{ route('app.invitation.index') . '?type=total-sent&order-by=created_at&order=desc' }}"
        >
        <div class="dashboard__card">
          <h2>Total Number Of Invitations That Have Been Sent</h2>
          <p>{{ $totalInvitationSent }}</p>
        </div>
      </a>
    </div>
  </div>
@endsection