@extends('app.layouts.index')

@section('title', 'Invitation | ' . env('APP_NAME'))

@section('content')
  <section class="invitation">
    <header class="card__header">
      <div class="container">
        @php
          $order = Request::get('order');
          $currentQueryString = Request::all();
          $queryString = [
            'order-by' => 'name',
            'order' => $order ? $order === 'desc' ? 'asc' : 'desc' : 'desc',
            'column' => 'email',
          ];
          $queryString = http_build_query(array_merge($currentQueryString, $queryString));
        @endphp

        <form action="{{ route('app.invitation.index') . '?' . $queryString }}" class="card__control">
          <input class="form-control inline" type="text" name="q" placeholder="lhildawan@gmail.com" value="{{ Request::get('q') }}" />
          <select name="column" id="column" class="form-control inline">
            <option value="email" {{ Request::get('column') === 'email' ? 'selected' : '' }}>Email</option>
            <option value="name" {{ Request::get('column') === 'name' ? 'selected' : '' }}>Name</option>
            <option value="registration_code" {{ Request::get('column') === 'registration_code' ? 'selected' : '' }}>Registration Code</option>
          </select>
          <button class="btn" type="submit">Search</button>
        </form>

        <a
          class="card__link"
          href="{{ route('app.invitation.create') }}"
        >
          Make an Invitation
        </a>
      </div>
    </header>

    <div class="container">
      <div class="card__content" style="overflow-x: auto;">
        <table>
          <thead>
            <tr>
              <th>#</th>
              <x-table-header label="Name" route="app.invitation.index" column="name" />
              <x-table-header label="Email" route="app.invitation.index" column="email" />
              <x-table-header label="Status" route="app.invitation.index" column="status" />
              <x-table-header label="Created At" route="app.invitation.index" column="created_at" />
              <x-table-header label="Updated At" route="app.invitation.index" column="updated_at" />
              <x-table-header label="Registered At" route="app.invitation.index" column="registered_at" />
              <th></th>
            </tr>
          </thead>
          <tbody>
            @if ($invitations->count())
              @foreach ($invitations as $key => $value)
                <tr>
                  <td>{{ $key + 1 }}</td>
                  <td>{{ $value->name ? $value->name : '-' }}</td>
                  <td>{{ $value->email }}</td>
                  <td>
                    <span class="invitation-status {{ strtolower($value->status->name) }}">{{ $value->status->name }}</span>
                  </td>
                  <td>{{ Carbon::parse($value->created_at)->diffForhumans() }}</td>
                  <td>{{ Carbon::parse($value->updated_at)->diffForhumans() }}</td>
                  <td>{{ $value->registered_at ? Carbon::parse($value->registered_at)->diffForhumans() : '-' }}</td>
                  <td>
                    <div class="actions">
                      <a href="{{ route('app.invitation.show', $value) }}" title="Details">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>
                      </a>
                      <a href="{{ route('app.invitation.edit', $value) }}" title="Reinvite">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-refresh-cw"><polyline points="23 4 23 10 17 10"></polyline><polyline points="1 20 1 14 7 14"></polyline><path d="M3.51 9a9 9 0 0 1 14.85-3.36L23 10M1 14l4.64 4.36A9 9 0 0 0 20.49 15"></path></svg>
                      </a>
                      <form action="{{ route('app.invitation.destroy', $value) }}" method="POST" onsubmit="return confirm('Are you sure you want to Remove?');">
                        @csrf
                        @method('DELETE')

                        <button type="submit" class="action-delete" title="Remove">
                          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                        </button>
                      </form>
                    </div>
                  </td>
                </tr>
              @endforeach
            @else
              <tr>
                <td colspan="7" class="no-data">There is no data</td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </section>
@endsection