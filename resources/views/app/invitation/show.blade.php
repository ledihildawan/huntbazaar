@extends('app.layouts.index')

@section('title', 'Invitation Details | ' . env('APP_NAME'))

@section('content')
  <section class="invitation">
    <header class="card__header">
      <div class="container">
        <h1 class="card__title">Invitation Details</h1>
      </div>
    </header>

    <div class="container">
      <div class="card__content">
        <div class="card__details">
          <p>Email</p>
          <p>{{ $invitation->email ? $invitation->email : '-' }}</p>
        </div>

        <div class="card__details">
          <p>Status</p>
          <p>{{ $invitation->status ? $invitation->status->name : '-' }}</p>
        </div>

        <div class="card__details">
          <p>Full Name</p>
          <p>{{ $invitation->name ? $invitation->name : '-' }}</p>
        </div>

        <div class="card__details">
          <p>Birth Date</p>
          <p>{{ $invitation->birth_date ? $invitation->birth_date : '-' }}</p>
        </div>

        <div class="card__details">
          <p>Gender</p>
          <p>{{ $invitation->gender ? ucfirst($invitation->gender) : '-' }}</p>
        </div>

        <div class="card__details">
          <p>Registration Code</p>
          <p>{{ $invitation->registration_code ? $invitation->registration_code : '-' }}</p>
        </div>

        <div class="card__details">
          <p>Designers Favorites</p>
          <p>{{ $invitation->designers_favorites ? $invitation->designers_favorites : '-' }}</p>
        </div>

        <div class="card__details">
          <p>Created At</p>
          <p>{{ $invitation->created_at ? $invitation->created_at : '-' }}</p>
        </div>

        <div class="card__details">
          <p>Updated At</p>
          <p>{{ $invitation->updated_at ? $invitation->updated_at : '-' }}</p>
        </div>

        <div class="card__details">
          <p>Registered At</p>
          <p>{{ $invitation->registered_at ? $invitation->registered_at : '-' }}</p>
        </div>
      </div>
    </div>
  </section>
@endsection