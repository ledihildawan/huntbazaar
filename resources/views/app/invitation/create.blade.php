@extends('app.layouts.index')

@section('title', 'Make an Invitation | ' . env('APP_NAME'))

@section('content')
  <section class="invitation">
    <header class="card__header">
      <div class="container">
        <h1 class="card__title">Make an Invitation</h1>
      </div>
    </header>

    <div class="container">
      <div class="card__content">
        <form
          action="{{ route('app.invitation.create') }}"
          method="POST"
        >
          @csrf

          <div class="form-group">
            <label for="email">Email:</label>
            <input
              class="form-control @if ($errors->has('email')) error @endif"
              type="email"
              id="email"
              name="email"
              placeholder="Email"
              value="{{ old('email') }}"
            />

            @error('email')
              <div class="text-error">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="expiration_date">Expiration Date:</label>
            <input
              class="form-control @if ($errors->has('expiration_date')) error @endif"
              type="datetime-local"
              name="expiration_date"
              id="expiration_date"
              placeholder="Expiration Date"
              value="{{ old('expiration_date') }}"
            />

            @error('expiration_date')
              <div class="text-error">{{ $message }}</div>
            @enderror
          </div>

          <button
            type="submit"
            class="btn"
          >
            Invite
          </button>
        </form>
      </div>
    </div>
  </section>
@endsection