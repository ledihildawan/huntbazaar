@extends('app.layouts.index')

@section('title', 'Make an Invitation | ' . env('APP_NAME'))

@section('content')
  <section class="invitation">
    <header class="card__header">
      <div class="container">
        <h1 class="card__title">Reinvite</h1>
      </div>
    </header>

    <div class="container">
      <div class="card__content">
        <form
          action="{{ route('app.invitation.update', $invitation) }}"
          method="POST"
        >
          @csrf
          @method('PATCH')

          <div class="form-group">
            <label for="email">Email:</label>
            <input
              class="form-control @if ($errors->has('email')) error @endif"
              type="email"
              id="email"
              name="email"
              placeholder="Email"
              value="{{ old('email', $invitation->email) }}"
            />
          </div>
          <div class="form-group">
            <label for="expiration_date">Expiration Date:</label>
            <input
              class="form-control @if ($errors->has('expiration_date')) error @endif"
              type="datetime"
              name="expiration_date"
              id="expiration_date"
              placeholder="Expiration Date"
              value="{{ old('expiration_date', $invitation->expiration_date) }}"
            />

            @error('expiration_date')
              <div class="text-error">{{ $message }}</div>
            @enderror
          </div>

          <button
            type="submit"
            class="btn"
          >
            Reinvite
          </button>
        </form>
      </div>
    </div>
  </section>
@endsection

@push('scripts')
  <script>
    const expirationDate = document.getElementById('expiration_date');

    if (expirationDate.type !== 'datetime-local') {
      expirationDate.addEventListener('click', function () {
        this.type = 'datetime-local';
      });
    }
  </script>
@endpush
