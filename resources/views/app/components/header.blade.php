<header class="header">
  <div class="container header__container">
    <div class="logo">
      @auth
        <a class="logo__text" href="{{ route('app.index') }}">{{ env('APP_NAME') }}</a>
      @endif

      @guest
        <a class="logo__text" href="{{ route('home') }}">{{ env('APP_NAME') }}</a>
      @endguest
    </div>

    <nav class="nav">
      <ul class="nav__list">
        @auth
          <li class="nav__list-item">
            <a class="nav__link" href="{{ route('app.invitation.index') }}">Invitation</a>
          </li>
          <li class="nav__list-item logout">
            <form action="{{ route('logout') }}" method="POST">
              @csrf

              <button>Logout</button>
            </form>
          </li>
        @endauth

        @guest
          <li class="nav__list-item">
            <a class="nav__link" href="{{ route('login') }}">Login</a>
          </li>
        @endguest
      </ul>
    </nav>
  </div>
</header>