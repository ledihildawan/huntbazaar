@extends('app.layouts.index')

@section('title', 'Login | ' . env('APP_NAME'))

@section('content')
  <section class="invitation">
    <header class="card__header">
      <div class="container">
        <h1 class="card__title">Login</h1>
      </div>
    </header>

    <div class="container">
      <div class="card__content">
        @if (Session::has('status'))
          <div>{{ Session::get('status') }}</div>
        @endif

        <form
          action="{{ route('login') }}"
          method="post"
        >
          @csrf

          <div class="form-group">
            <label for="email">Email</label>
            <input
              class="form-control @if ($errors->has('email')) error @endif"
              type="email"
              name="email"
              id="email"
              placeholder="Email"
            />

            @error('email')
              <div class="text-error">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="password @if ($errors->has('password')) error @endif">Password</label>
            <input
              class="form-control @if ($errors->has('email')) error @endif"
              type="password"
              name="password"
              id="password"
              placeholder="Password"
            />

            @error('password')
              <div class="text-error">{{ $message }}</div>
            @enderror
          </div>

          <button type="submit" class="btn">Login</button>
        </form>
      </div>
    </div>
  </section>
@endsection