@component('mail::message')

Good {{ $timeOfDay }} {{ $invitation->name }}, you have registered as an invitee to attend the HUNTBAZAAR event on December 12, 2021. Here is your registration code: {{ $invitation->registration_code }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
