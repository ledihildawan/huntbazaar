@component('mail::message')
Good {{ $timeOfDay }}, we invite you to the HUNTBAZAAR event which will be held on December 12, 2021. Click the button or follow the link to register.

@component('mail::button', ['url' => $url])
Open Invitation
@endcomponent

[{{ $url }}]({{ $url }})

Thanks,<br>
{{ config('app.name') }}
@endcomponent
