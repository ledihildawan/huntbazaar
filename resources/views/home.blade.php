@extends('app.layouts.index')

@section('title', env('APP_NAME'))

@section('content')

@endsection

@push('styles')
  <style>
    body {
      background: url('/bg-home.jpg') center/cover no-repeat;
      height: 100vh;
    }
  </style>
@endpush