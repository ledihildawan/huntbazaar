@extends('app.layouts.index')

@section('title', 'Registartion Event | ' . env('APP_NAME'))

@section('content')
  <section class="invitation">
    <header class="card__header">
      <div class="container">
        @if ($isRegistrationStillOpen && !$invitation->registered_at)
          <h1 class="card__title">Register Event</h1>
        @endif
      </div>
    </header>

    <div class="container">
      <div class="card__content">
        @if ($isRegistrationStillOpen && !$invitation->registered_at)
        <div class="countdown">
          <span id="countdown"></span>
        </div>

        <form action="{{ route('invitation.link', $invitation) }}" method="POST">
          @csrf
          @method('PATCH')

          <div class="form-group">
            <label for="email">Email:</label>
            <input
              class="form-control @if ($errors->has('email')) error @endif"
              type="email"
              name="email"
              placeholder="Email"
              readonly
              value="{{ old('email', $invitation->email) }}"
            />

            @error('email')
              <div class="text-error">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="name">Full Name:</label>
            <input
              class="form-control @if ($errors->has('name')) error @endif"
              type="name"
              name="name"
              placeholder="Full Name"
              value="{{ old('name', $invitation->name) }}"
            />

            @error('name')
              <div class="text-error">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="birth_date">Birth Date:</label>
            <input
              class="form-control @if ($errors->has('birth_date')) error @endif"
              type="date"
              name="birth_date"
              placeholder="Birth Date"
              value="{{ old('birth_date', $invitation->birth_date) }}"
            />

            @error('birth_date')
              <div class="text-error">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="gender">Gender:</label>
            <select
              name="gender"
              id="gender"
              class="form-control @if ($errors->has('gender')) error @endif"
            >
              <option value="female" {{ old('gender', $invitation->gender) === 'female' ? 'selected' : '' }}>Female</option>
              <option value="male" {{ old('gender', $invitation->gender) === 'male' ? 'selected' : '' }}>Male</option>
            </select>

            @error('gender')
              <div class="text-error">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="gender">Designers Favorites:</label>
            <select
              name="designers_favorites[]"
              id="gender"
              class="form-control @if ($errors->has('designers_favorites')) error @endif"
              multiple
              size="4"
            >
              @foreach ($designers as $designer)
                <option value="{{ $designer->name }}">{{ $designer->name }}</option>
              @endforeach
            </select>

            @error('designers_favorites')
              <div class="text-error">{{ $message }}</div>
            @enderror
          </div>
          <button
            type="submit"
            class="btn"
          >
            Register
          </button>
        </form>
        @else
          @if ($invitation->registered_at)
            <h1>You are already registered for this event, here is the registration code: {{ $invitation->registration_code }}</h1>
          @else
            <h1>Registration for this event has been closed.</h1>
          @endif
        @endif
      </div>
    </div>
  </section>

  @if ($isRegistrationStillOpen && !$invitation->registered_at)
    <script>
      countDownTimer('{{ $invitation->created_at }}', 'countdown');

      function countDownTimer(dt, id) {
        let end = new Date('{{$invitation->expiration_date}}');
        let _second = 1000;
        let _minute = _second * 60;
        let _hour = _minute * 60;
        let _day = _hour * 24;
        let timer;

        function showRemaining() {
          let now = new Date();
          let distance = end - now;

          if (distance < 0) {
            clearInterval(timer);

            document.getElementById(id).closest('.countdown').innerHTML = 'Registration for this event has been closed.';

            location.reload();

            return;
          }

          let days = Math.floor(distance / _day);
          let hours = Math.floor((distance % _day) / _hour);
          let minutes = Math.floor((distance % _hour) / _minute);
          let seconds = Math.floor((distance % _minute) / _second);

          document.getElementById(id).closest('.countdown').innerHTML = 'The rest of the time to register for this event: <span id="countdown"></span>';

          document.getElementById(id).innerHTML = days ? days + ' days ' : '';
          document.getElementById(id).innerHTML += hours ? hours + ' hrs ' : '';
          document.getElementById(id).innerHTML += minutes ? minutes + ' mins ' : '';
          document.getElementById(id).innerHTML += seconds + ' secs';
        }

        timer = setInterval(showRemaining, 1000);
      }
    </script>
  @endif
@endsection