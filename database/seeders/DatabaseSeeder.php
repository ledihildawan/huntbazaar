<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Status;
use App\Models\Designer;

class DatabaseSeeder extends Seeder
{
    public $designers = [
        'A Bathing Ape',
        'Aesop',
        'fbudi',
        'Simone Rocha X H&M',
        'Tracy Reese',
        'Van Cleef & Arpels',
        'Bottega Veneta',
    ];

    public $statues = [
        'Sent',
        'Registered',
    ];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create();

        foreach ($this->designers as $designer) {
            Designer::factory()->create(['name' => $designer]);
        }

        foreach ($this->statues as $status) {
            Status::factory()->create(['name' => $status]);
        }
    }
}
