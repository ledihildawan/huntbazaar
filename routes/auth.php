<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;

Route::group(['prefix' => 'login', 'middleware' => ['guest']], function () {
  Route::get('/', [LoginController::class, 'index'])->name('login');
  Route::post('/', [LoginController::class, 'store']);
});

Route::post('/logout', [LogoutController::class, 'store'])->name('logout');