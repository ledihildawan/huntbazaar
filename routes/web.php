<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\InvitationController;

Route::get('/', function () {
    return view('home');
})->name('home');

include_once __DIR__ . '/auth.php';

Route::group(['prefix' => 'invitation'], function () {
    Route::get('/', function () {
        return redirect('/');
    });

    Route::get('/{invitation:link}', [InvitationController::class, 'invitation'])->name('invitation.link');
    Route::patch('/{invitation:link}', [InvitationController::class, 'invitationUpdate']);
});

Route::group([
    'prefix' => 'app',
    'middleware' => ['auth']], function () {

    Route::get('/', [DashboardController::class, 'index'])->name('app.index');

    Route::group(['prefix' => 'invitation'], function () {
        Route::delete('/{invitation}', [InvitationController::class, 'destroy'])->name('app.invitation.destroy');

        Route::get('/', [InvitationController::class, 'index'])->name('app.invitation.index');
        Route::get('/make-an-invitation', [InvitationController::class, 'create'])->name('app.invitation.create');
        Route::get('/{invitation}', [InvitationController::class, 'show'])->name('app.invitation.show');
        Route::get('/{invitation}/edit', [InvitationController::class, 'edit'])->name('app.invitation.edit');

        Route::patch('/{invitation}', [InvitationController::class, 'update'])->name('app.invitation.update');

        Route::post('/make-an-invitation', [InvitationController::class, 'store']);
    });
});
